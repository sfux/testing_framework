#!/bin/bash

#######################################################################
# Script to test basic functionality of software on the Euler cluster #
# Authors: Samuel Fux, Jarunan Panyasantisuk                          #
# 2020 @ ETH Zurich                                                   #
#######################################################################

#############
# Variables #
#############

# run tests on current node
RUN_LOCAL=0
# run tests through the batch sytem
RUN_BATCHSYSTEM=0
# show available tests
SHOW_AVAILABLE_TESTS=0

# list of subdirectories containing the tests
declare -a directories=("abaqus" "adf" "cfx" "comsol" "numpy" "mathematica" "matlab" "tensorflow")

# function to display help message and usage
function display_help {
cat <<-EOF
$0: Script to run testing framework

Usage:                  testing_framework.sh -h -s -l -b\n

Parameters:

 -h | --help            Show help and exit
 -s | --show            Show available tests and exit
 -l | --local           Run tests on current node
 -b | --batchsystem     Run tests through batch sytem

Example:
./testing_framework.sh --show

EOF
exit 1
}

# Parse command line arguments:
while [[ $# -gt 0 ]]
do
        case $1 in
                -h|--help)
                shift
                display_help
                ;;
                -l|--local)
                RUN_LOCAL=1
                shift
                ;;
                -b|--batchsystem)
                RUN_BATCHSYSTEM=1
                shift
                ;;
                -s|--show)
                SHOW_AVAILABLE_TESTS=1
                shift
                ;;
                *)
                echo -e "Warning: ignoring unknown option $1 \n"
                shift
                ;;
        esac
done

if [ "$SHOW_AVAILABLE_TESTS" = "1" ]; then
    # Iterate over directories
    echo -e "Available tests:\n"
    for dir in ${directories[@]}; do
        echo -e "* $dir"
    done
    echo -e " "
    exit 1
fi

if [ "$RUN_LOCAL" = "1" ]
then
    # Iterate over directories
    for dir in ${directories[@]}; do
        echo -e "Entering directory $dir"
        cd $dir
        echo -e "Submitting testjob for $dir"
        ./test.sh LOCAL
        cd ..
    done
elif [ "$RUN_BATCHSYSTEM" = "1" ]
then
    # Iterate over directories
    for dir in ${directories[@]}; do
        echo -e "Entering directory $dir"
        cd $dir
        echo -e "Submitting testjob for $dir"
        ./test.sh SLURM
        cd ..
    done
fi
