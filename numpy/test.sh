#!/bin/bash

# switch to new software stack
source /cluster/apps/local/env2lmod.sh

# unload all modules and load required ones
module purge
module load StdEnv
module load gcc/8.2.0 python/3.10.4

# read global LSF options
GLOBAL_BSUB_OPTIONS=`cat ../global_bsub_options`
GLOBAL_SBATCH_OPTIONS=`cat ../global_sbatch_options`

case $1 in
    LOCAL)
	# run numpy test local
        python test_numpy.py &> ../logs/numpy.errout
    ;;
    SLURM)
        # run numpy test as batch job in Slurm
        sbatch --ntasks=1 --time=1:00:00 --mem-per-cpu=1g --output=../logs/numpy.out --error=../logs/numpy.err $GLOBAL_SBATCH_OPTIONS --wrap="python ./test_numpy.py"
    ;;
    LSF)
        # run numpy test as batch job in LSF
        bsub -n 1 -W 1:00 -R "rusage[mem=1024]" -oo ../logs/numpy.out -eo ../logs/numpy.err $GLOBAL_BSUB_OPTIONS python ./test_numpy.py
    ;;
    *)
	echo -e "Something went wrong"
    ;;
esac
