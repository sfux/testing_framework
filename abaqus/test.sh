#!/bin/bash

# switch to new software stack
source /cluster/apps/local/env2lmod.sh

# unload all modules and load required ones for running MY_PROGRAM
module purge
module load StdEnv
module load intel/19.1.0 abaqus/2020

# read global options
GLOBAL_BSUB_OPTIONS=`cat ../global_bsub_options`
GLOBAL_SBATCH_OPTIONS=`cat ../global_sbatch_options`

case $1 in
    LOCAL)
        # run ADF test local
        abaqus cpus=1 job=job1 inp=boltpipeflange_axi_solidgask.inp &> ../logs/abaqus.errout
    ;;
    SLURM)
        # run Abaqus test as batch job in Slurm
        NPROCS=2
        sbatch --ntasks=$NPROCS --time=1:00:00 --mem-per-cpu=1g --output=../logs/abaqus.out --error=../logs/abaqus.err $GLOBAL_SBATCH_OPTIONS --wrap="abaqus cpus=$NPROCS job=job1 inp=boltpipeflange_axi_solidgask.inp scratch=\$TMPDIR"
    ;;
    LSF)
        # run Abaqus test as batch job in LSF
        NPROCS=2
        bsub -n $NPROCS -W 1:00 -R "rusage[mem=1024]" -oo ../logs/abaqus.out -eo ../logs/abaqus.err $GLOBAL_BSUB_OPTIONS abaqus cpus=$NPROCS job=job1 inp=boltpipeflange_axi_solidgask.inp scratch=\$TMPDIR
    ;;
    *)
        echo -e "Something went wrong"
    ;;
esac
