# Testing_framework

This project contains a script testing_framework.sh to test basic functionality of software on the Euler cluster (local or through the batch sytem). Each directory contains a simple test for a software and a script test.sh to run the test and will write stderr and stdout to the log directory.

## Clone the repository

The repository can be cloned with the command

`git clone https://gitlab.ethz.ch/sfux/testing_framework.git`

```
git clone https://gitlab.ethz.ch/sfux/testing_framework.git
Cloning into 'testing_framework'...
remote: Enumerating objects: 3, done.
remote: Counting objects: 100% (3/3), done.
remote: Compressing objects: 100% (3/3), done.
remote: Total 126 (delta 0), reused 0 (delta 0), pack-reused 123
Receiving objects: 100% (126/126), 43.30 KiB | 0 bytes/s, done.
Resolving deltas: 100% (55/55), done.
```

## Usage

```
./testing_framework.sh --help
./testing_framework.sh: Script to run testing framework

Usage:                  testing_framework.sh -h -s -l -b\n

Parameters:

 -h | --help            Show help and exit
 -s | --show            Show available tests and exit
 -l | --local           Run tests on current node
 -b | --batchsystem     Run tests through batch sytem

Example:
./testing_framework.sh --show
```

```
[sfux@eu-login-99 testing_framework]$ ./testing_framework.sh --show
Available tests:

* numpy
* matlab
* tensorflow
* mathematica
* adf

[sfux@eu-login-99 testing_framework]$
```

## Adding new tests
- Create directory with a self-explanatory name (e.g., adf_serial)
- Add input and data files that are required for the test to the directory
- Add test.sh script, which takes the options LSF or local

Template (for the test MY_PROGRAM):

```
#!/bin/bash

# switch to new software stack
source /cluster/apps/local/env2lmod.sh

# unload all modules and load required ones for running MY_PROGRAM
module purge
module load StdEnv
module load ...

# read global LSF options
GLOBAL_BSUB_OPTIONS=`cat ../global_bsub_options`

case $1 in
    LOCAL)
        # run MY_PROGRAM test local
        __COMMAND_TO_BE_RUN_LOCALLY__
    ;;
    LSF)
        # run MY_PROGRAM test as batch job (adapt LSF settings if required)
        bsub -n 1 -W 1:00 -R "rusage[mem=1024]" -oo ../logs/MY_PROGRAM.out -eo ../logs/MY_PROGRAM.err $GLOBAL_BSUB_OPTIONS __COMMAND_TO_BE_RUN_LOCALLY__
    ;;
    *)
        echo -e "Something went wrong"
    ;;
esac
```


## Tests
TODO: Add tests for the following programs

- Abaqus
- ADF
- Comsol
- Mathematica
- Maple
- Some Ansys Software
- Tensorflow