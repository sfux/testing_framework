#!/bin/bash

# switch to new software stack
source /cluster/apps/local/env2lmod.sh

# unload all modules and load required ones
module purge
module load StdEnv
module load adf/2023.101

# read global options
GLOBAL_BSUB_OPTIONS=`cat ../global_bsub_options`
GLOBAL_SBATCH_OPTIONS=`cat ../global_sbatch_options`

case $1 in
    LOCAL)
        # run ADF test local
        ./h2o.run &> ../logs/adf.errout
    ;;
    SLURM)
        # run ADF test as batch job in Slurm
        sbatch --ntasks=4 --time=0:20:00 --mem-per-cpu=2g --output=../logs/adf.out --error=../logs/adf.err $GLOBAL_SBATCH_OPTIONS < h2o.run
    ;;
    LSF)
        # run ADF test as batch job in LSF
        bsub -n 4 -W 0:20 -R "rusage[mem=2048]" -oo ../logs/adf.out -eo ../logs/adf.err $GLOBAL_BSUB_OPTIONS < h2o.run 
    ;;
esac


