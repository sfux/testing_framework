#!/bin/bash

# switch to new software stack
source /cluster/apps/local/env2lmod.sh

# unload all modules and load required ones
module purge
module load StdEnv
module load mathematica/13.0.1

# read global options
GLOBAL_BSUB_OPTIONS=`cat ../global_bsub_options`
GLOBAL_SBATCH_OPTIONS=`cat ../global_sbatch_options`

case $1 in
    LOCAL)
        # run mathematica test local
        math -script test.m &> ../logs/mathematica.errout
    ;;
    SLURM)
        # run mathematica test as batch job in Slurm
        sbatch --ntasks=4 --time=0:30:00 --mem-per-cpu=1g --output=../logs/mathematica.out --error=../logs/mathematica.err $GLOBAL_SBATCH_OPTIONS math -script test.m
    ;;
    LSF)
        # run mathematica test as batch job in LSF
        bsub -n 4 -W 0:30 -R "rusage[mem=1024]" -oo ../logs/mathematica.out -eo ../logs/mathematica.err $GLOBAL_BSUB_OPTIONS math -script test.m
    ;;
esac


