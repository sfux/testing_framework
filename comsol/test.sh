#!/bin/bash

# switch to new software stack
source /cluster/apps/local/env2lmod.sh

# unload all modules and load required ones for running MY_PROGRAM
module purge
module load StdEnv
module load comsol/6.1

# read global options
GLOBAL_BSUB_OPTIONS=`cat ../global_bsub_options`
GLOBAL_SBATCH_OPTIONS=`cat ../global_sbatch_options`

case $1 in
    LOCAL)
        # run MY_PROGRAM test local
        comsol batch -np 1 -inputfile BeamModel.mph -outputfile outfile.mph &> ../logs/comsol.errout
    ;;
    SLURM)
        # run comsol test as batch job in Slurm
        NPROCS=2
        sbatch --ntasks=$NPROCS --time=1:00:00 --mem-per-cpu=1g --output=../logs/comsol.out --error=../logs/comsol.err $GLOBAL_SBATCH_OPTIONS --wrap="comsol batch -np $NPROCS -inputfile BeamModel.mph -outputfile outfile.mph"
    ;;
    LSF)
        # run comsol test as batch job in LSF
        NPROCS=2
        bsub -n $NPROCS -W 1:00 -R "rusage[mem=1024]" -oo ../logs/comsol.out -eo ../logs/comsol.err $GLOBAL_BSUB_OPTIONS comsol batch -np $NPROCS -inputfile BeamModel.mph -outputfile outfile.mph
    ;;
    *)
        echo -e "Something went wrong"
    ;;
esac
