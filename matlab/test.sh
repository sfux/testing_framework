#!/bin/bash

# switch to new software stack
source /cluster/apps/local/env2lmod.sh

# unload all modules and load required ones
module purge
module load StdEnv
module load matlab/R2022b

# read global options
GLOBAL_BSUB_OPTIONS=`cat ../global_bsub_options`
GLOBAL_SBATCH_OPTIONS=`cat ../global_sbatch_options`

case $1 in
    LOCAL)
        # run matlab test local
        matlab -nodisplay -nojvm -singleCompThread -r test &> ../logs/matlab.errout
    ;;
    SLURM)
        # run matlab test as batch job in Slurm
        sbatch --ntasks=1 --time=1:00:00 --mem-per-cpu=1g --output=../logs/matlab.out --error=../logs/matlab.err $GLOBAL_SBATCH_OPTIONS --wrap="matlab -nodisplay -nojvm -singleCompThread -r test"
    ;;
    LSF)
        # run matlab test as batch job in LSF
        bsub -n 1 -W 1:00 -R "rusage[mem=1024]" -oo ../logs/matlab.out -eo ../logs/matlab.err $GLOBAL_BSUB_OPTIONS matlab -nodisplay -nojvm -singleCompThread -r test
    ;;
esac


