#!/bin/bash

# switch to new software stack
source /cluster/apps/local/env2lmod.sh

# unload all modules and load required ones for running MY_PROGRAM
module purge
module load StdEnv
module load cfx/21.2_research

# read global options
GLOBAL_BSUB_OPTIONS=`cat ../global_bsub_options`
GLOBAL_SBATCH_OPTIONS=`cat ../global_sbatch_options`

case $1 in
    LOCAL)
        # run MY_PROGRAM test local
        cfx5solve -def StaticMixer.def &> ../logs/comsol.errout
    ;;
    SLURM)
        # run cfx test as batch job in Slurm
        cfx_submit_job -b Slurm -n 2 -W 4:00 -M 2000 -def StaticMixer.def
    ;;
    LSF)
        # run cfx test as batch job in LSF
        cfx_submit_job -b LSF -n 2 -W 4:00 -M 2000 -def StaticMixer.def
    ;;
    *)
        echo -e "Something went wrong"
    ;;
esac
