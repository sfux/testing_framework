#!/bin/bash

# switch to new software stack
source /cluster/apps/local/env2lmod.sh

# unload all modules and load required ones for running tensorflow
module purge
module load StdEnv
module load gcc/8.2.0 eth_proxy python/3.10.4

# read global options
GLOBAL_BSUB_OPTIONS=`cat ../global_bsub_options`
GLOBAL_BSUB_OPTIONS=`cat ../global_sbatch_options`


case $1 in
    LOCAL)
        # run tensorflow test local
        python train_mnist.py &> ../logs/tensorflow.errout
    ;;
    SLURM)
        # run tensorflow test as batch job in Slurm
        sbatch --ntasks=1 --time=1:00:00 --mem-per-cpu=2g --output=../logs/tensorflow.out --error=../logs/tensorflow.err $GLOBAL_SBATCH_OPTIONS --wrap="python train_mnist.py"
    ;;
    LSF)
        # run tensorflow test as batch job in LSF
        bsub -n 1 -W 1:00 -R "rusage[mem=1024]" -oo ../logs/tensorflow.out -eo ../logs/tensorflow.err $GLOBAL_BSUB_OPTIONS python train_mnist.py
    ;;
    *)
        echo -e "Something went wrong"
    ;;
esac
